# PN Syst - Script di installazione nuova istanza Liferay (pn-install-esi-instance)

Vedi [documentazione pn-install-esi-instance](https://peernetwork.atlassian.net/wiki/x/YADPng)

# esi-deploy
## Introduzione

Questo script è stato realizzato per automatizzare tutta una serie di procedure che spesso vengono fatte in maniera manuale o semimanuale dagli sviluppatori che intendono caricare, deployare, riavviare e consultare i log di ESI su un sistema cliente.

## Prerequisiti

Per utilizzare questo script è necessario avere sul proprio sistema anche gli scripts presenti su `scripts/ssh-scripts` e avere eseguito il comando da `root` o `sudoer` `./load-pnssh.sh`.

Vedere la documentazione del relativo modulo per maggiori informazioni.

Inoltre è necessario disporre del collegamento VPN attivo verso il cliente desiderato e avere la propria chiave pubblica RSA caricata fra gli `authorized_keys` del server cliente.

Infine occorre aver definito la variabile d'ambiente utente `ESI_BUILDS_PATH` nel file `~/.profile` come path in cui vengono esportate le build ESI (e.g. `/home/dfabbri/esi-builds`).

## Installazione

Per poter disporre di `esi-deploy` da qualsiasi PATH in cui ci trovi occorre dapprima caricarlo tramite lo script di caricamento eseguibile tramite il comando `sudo ./load-esideploy.sh` e fornendo le proprie credenziali di utente `sudoer`.

## Utilizzo

Si può visualizzare la modalità di utilizzo dello script tramite il comando:
`esi-deploy -h`

Il pattern di utilizzo dello script è il seguente:
`esi-deploy [-u|d|r|l|h] customerlevel`

Dove `customerlevel` è il nome del cliente seguito dall'istanza a cui si fa riferimento seguendo i landscape definiti per tale cliente. (e.g. `baykerbyr`, `florimusaprd` , `montetest` )

Le opzioni principali sono le seguenti

### Upload
`-u` Carica il war nella cartella `esi-builds` dell'istanza Tomcat di riferimento e ne fa una copia denominata `ESI.war`. Prima di effettuare questo upload, effettua un backup dell'ultima versione deployata copiandola in `ESI_OLD.war`

### Deploy
`-d` Copia `ESI.war` dalla cartella `esi-builds` dell'istanza Tomcat di riferimento nella relativa cartella `webapps`.

### Restart
`-r` Riavvia l'istanza `ESI` di riferimento

### Log
`-l` Mostra il log dell'istanza Tomcat di riferimento lanciando il comando `esi-log-***` mostrando l'output sulla console corrente. Premendo Ctrl+C la console "torna" sul sistema locale dove è stato eseguito `esi-deploy`


