#!/bin/bash



if [[ $EUID -ne 0 ]]; then
	echo "This script must be run as root. Aborting"
	exit 1
fi





function read_variable {
	local value=""
	echo "$1 ($2):"
	read $3 value
	read_variable_result=${value:-$2}
}



read_variable "Insert customer name in lowercase (e.g. peernetwork)" ""
customer_name="$read_variable_result"
echo

read_variable "Insert ref landscape level in lowercase (e.g. prd, test, dev)" "prd"
landscape_level="$read_variable_result"
echo



os_username=$1
os_username=${os_username:-root}
read_variable "Insert OS user for docker management" "$os_username"
os_username="$read_variable_result"
echo


#base_folder=`dirname "$(readlink -f "$0")"`
read_variable "Insert new esi3 installation folder destination" "/opt"
base_folder="$read_variable_result"
echo

read_variable "Insert new esi3 installation files directory" "$base_folder/esi3-installation-files"
installation_files_folder="$read_variable_result"
echo

db_username=$1
db_username=${db_username:-root}
read_variable "Insert mysql user" "$db_username"
db_username="$read_variable_result"
echo

db_password=$2
db_password=${db_password:-}
read_variable "Insert mysql $db_username's password" "$db_password" "-s"
db_password="$read_variable_result"
echo

read_variable "Insert mysql schema" "esi3_$customer_name$landscape_level"
db_schema="$read_variable_result"
echo


read_variable "Insert new installation folder suffix" "-$landscape_level"
esi_folder_suffix="$read_variable_result"
echo



esi_folder="esi3$esi_folder_suffix"
installation_folder="$base_folder/$esi_folder"

mkdir -p $installation_folder/config




echo "
x-pn-definitions:
  ports: &ports
    ports:
      - "8080"
  volumes: &volumes
    volumes:
      - ./config:/pn-conf
  dependencies: &dependencies
    depends_on:
      load-balancer:
        condition: service_healthy
  restart: &restart
    restart: unless-stopped
  healthcheck: &healthcheck
    healthcheck:
      test: curl -f http://localhost:8080/module-status || exit 1
      interval: 20s
      timeout: 10s
      retries: 3
      start_period: 30s
  deploy: &deploy
    deploy:
      mode: replicated
      replicas: 1
  default-cfg: &default-cfg
    <<: [ *ports, *volumes, *dependencies, *restart, *healthcheck, *deploy ]

services:
  esi-mysql:
    image: mysql:8.0.39
    ports:
      - 3306:3306
    restart: unless-stopped
    environment:
      MYSQL_ROOT_PASSWORD: $db_password
      MYSQL_ROOT_HOST: "%"
      MYSQL_USER: $db_username
      MYSQL_PASSWORD: $db_password
      MYSQL_DATABASE: liferay
    volumes:
      - ./mysql/data:/var/lib/mysql
    healthcheck:
      test: mysqladmin ping -h 127.0.0.1 -u root -p$db_password || exit 1
      timeout: 5s
      retries: 5
  load-balancer:
    image: pnpeernetwork/rest-load-balancer:0.1.0
    ports:
      - "8090:8080"
    volumes:
      - ./config:/pn-conf
      - ./config/load-balancer:/module-conf
    depends_on:
      esi-mysql:
        condition: service_healthy
    <<: [ *restart, *healthcheck ]
  api-gateway:
    image: pnpeernetwork/rest-api-gateway:0.1.0
    ports:
      - "8080:8080"
    volumes:
      - ./config:/pn-conf
      - ./config/api-gateway:/module-conf
    <<: [ *restart, *healthcheck, *dependencies ]
  pn-core-admin:
    image: pnpeernetwork/esi3-pn-core-admin-saps4:0.1.0
    <<: *default-cfg
" >> $installation_folder/compose.yaml



mysql -u $db_username -p$db_password -e "CREATE SCHEMA $db_schema DEFAULT CHARACTER SET utf8;"




ctl_command="/usr/bin/esi3-ctl$esi_folder_suffix"
cp $installation_files_folder/esi3-ctl-xxx.template $ctl_command
sed -i -Ee "s+^(#\s*Provides.*)(esi3)+\1\2$esi_folder_suffix+g" $ctl_command
sed -i -Ee "s+^(#\s*Short-Description.*)(esi3)(.*)+\1\2$esi_folder_suffix\3+g" $ctl_command
sed -i -Ee "s+^(INSTALLATION_FOLDER=)+\1$installation_folder+g" $ctl_command
vim $ctl_command
chmod +x $ctl_command





log_command="/usr/bin/esi3-log$esi_folder_suffix"
echo "#!/bin/sh

docker compose -f $installation_folder/compose.yaml logs -f \$1
" > $log_command
vim $log_command
chmod +x $log_command





oom_score_adjust=$([ "$esi_folder_suffix" = "prd" ] && echo "-1000" || echo "-750")
service_file="/etc/systemd/system/esi3$esi_folder_suffix.service"
echo "[Unit]
Description=ESI-3 - esi3$esi_folder_suffix
After=network.target
#After=mysql.target

[Service]
Type=forking
RemainAfterExit=yes
User=$os_username
ExecStart=$ctl_command start
ExecReload=$ctl_command restart
ExecStop=$ctl_command stop
OOMScoreAdjust=$oom_score_adjust

[Install]
WantedBy=multi-user.target
" > $service_file
vim $service_file
systemctl enable esi3$esi_folder_suffix




echo ""
echo ""
while true; do
	read -p "Change owner of $installation_folder to $os_username [Y/n]? " change_owner
	case $change_owner in
		[Nn]* )
			break;;
		""|[Yy]* )
			chown -R $os_username:root $installation_folder
			break;;
		* )
			echo "Please answer y or n.";;
	esac
done


echo ""
echo ""
while true; do
	read -p "Initialize mysql schema $db_schema? [Y/n]" initialize_schema
	case $initialize_schema in
		[Nn]* )
			break;;
		""|[Yy]* )
			mysql -u $db_username -p$db_password $db_schema < $installation_files_folder/esi-schema-initializer.sql
			break;;
		* )
			echo "Please answer y or n.";;
	esac
done


echo ""
echo ""
# https://serverfault.com/questions/112795/how-to-run-a-server-on-port-80-as-a-normal-user-on-linux
# iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 8080
while true; do
	read -p "Enable port redirecting 8080 -> 80 [Y/n]? " enable_port_redirect
	case $enable_port_redirect in
		[Nn]* )
			break;;
		""|[Yy]* )
			apt -y install iptables-persistent
			iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 8080
			iptables-save > /etc/iptables/rules.v4
			iptables-save > /etc/iptables/rules.v6
			break;;
		* )
			echo "Please answer y or n.";;
	esac
done



echo ""
echo ""
while true; do
	read -p "Start ESI now [Y/n]? " start_esi
	case $start_esi in
		[Nn]* )
			break;;
		""|[Yy]* )
			systemctl start esi3$esi_folder_suffix
			$log_command
			break;;
		* )
			echo "Please answer y or n.";;
	esac
done