# PN Syst - Script di upgrade versione Liferay (pn-update-lr)

Vedi [documentazione pn-update-lr](https://peernetwork.atlassian.net/wiki/x/FQDrng)

# pn-update-lr

## Introduzione

Questo script è stato realizzato per automatizzare l'aggiornamento delle installazioni di LR

## Prerequisiti

- Lo script deve essere caricato sulla macchina dove è installata l'istanza di LR da aggiornare
- Viene utilizzato MySql come DB (per ora abbiamo previsto solo questo)

## Utilizzo
Lanciare lo script come un normale file eseguibile (solitamente `./pn-update-lr`)
Verranno richieste alcune informazioni per poter proseguire
* Cartella dove installare la nuova istanza di LR (solitamente `/opt`, default la cartella dove si trova lo script). Da ora in avanti questa cartella verrà chiamata `base_folder`
* Cartella dove cercare/scaricare l'installer della nuova istanza di LR (solitamente `/opt/installers`, default `base_folder/installers`)
* Versione della nuova instalza di LR
* Cartella dell'istanza di LR da aggiornare (solitamente `/opt/liferay-prd`, default `base_folder/liferay-prd`)
* Link simbolico per la nuova instalza di LR (solitamente `/opt/liferay-prd`, default `base_folder/liferay-prd`)
* Utente, password e nome schema di MySql per il backup del database

Una volta inserite tutte le informazioni necessarie lo script procede a
* Spegnere la vecchia istanza di LR. Visto che il tempo necessario per questa operazione è variabile e il prompt viene liberato subito, è necessario premere invio per continuare
* Eseguire il backup del database. Il file `.sql` verrà memorizzato nella stessa cartella dove è presente lo script. Se per un qualunque motivo il backup non dovesse andare a buon fine, lo script si arresterebbe
* Download del file `tar.gz` contenente l'installer della versione di LR richiesta. Questo step viene saltato se tale file è già presente nella cartella indicata
* Decompressione archivio con la nuova versione di LR
* Creazione del link simbolico `tomcat` all'interno della nuova cartella di LR
* Copia dei file della vecchia installazione in quella nuova. Questi file comprendono
  * la cartella `data`
  * la cartella `tomcat/pn-props`
  * il file `portal-ext.properties`
  * il file `server.xml` (del quale verrà prima fatto un backup)
  * il file `portal-setup-wizard.properties` (al quale verrà modificata la prop `liferay.home` impostando quella corretta per la nuova installazione)
* Creazione del link simbolico alla nuova istanza di LR
* Raccolta delle informazioni necessarie per la procedura di upgrade dello schema
* Avvio dello script di upgrade dello schema di LR (previa conferma dell'utente)
* Avvio della nuova istanza di LR (previa conferma dell'utente)