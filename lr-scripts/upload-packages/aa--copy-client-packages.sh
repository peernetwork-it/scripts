WORKING_FOLDER=$(dirname $(readlink -f $0))/
CLIENT_NAME=$1
EDITION=$2
VERSION=$3
USER=$4
SERVER_IP=$5
REMOTE_FOLDER_PATH=$6

PKGS_LIST=""
while shift && [ -n "$6" ]; do
	PKGS_LIST=$PKGS_LIST" "$6
done

PKGS_LIST="$PKGS_LIST \
pn--call-esi-service-$VERSION.jar \
pn--external-url-services-$VERSION.jar \
pn--hooks-$VERSION.jar \
pn--http-client-$VERSION.jar \
pn--layouts-admin-services-$VERSION.jar \
pn--organizations-admin-services-$VERSION.jar \
pn--portal-usage-api-$VERSION.jar \
pn--portal-usage-service-$VERSION.jar \
pn--portal-usage-services-$VERSION.jar \
pn--portal-usage-portlets-$VERSION.jar \
pn--roles-admin-services-$VERSION.jar \
pn--asset-tags-admin-services-$VERSION.jar \
pn--sites-menu-$VERSION.jar \
pn--users-admin-$VERSION.jar \
pn--users-admin-services-$VERSION.jar"



cd $WORKING_FOLDER

bash $WORKING_FOLDER/aa--copy-packages.sh
echo $PKGS_LIST
echo "Copying packages of version $EDITION $VERSION to $CLIENT_NAME -> $SERVER_IP:$REMOTE_FOLDER_PATH"

cd $EDITION-$VERSION
rsync -uh --progress $PKGS_LIST $USER@$SERVER_IP:$REMOTE_FOLDER_PATH
# rsync -uhW --progress $PKGS_LIST $USER@$SERVER_IP:$REMOTE_FOLDER_PATH
# scp $PKGS_LIST $USER@$SERVER_IP:$REMOTE_FOLDER_PATH

while true; do
	read -p "Start ssh connection [Y/n]? " startSshConn
	case $startSshConn in
		[Nn]* ) break;;
		""|[Yy]* ) ssh $USER@$SERVER_IP; break;;
		* ) echo "Please answer y or n.";;
	esac
done

exit 0
