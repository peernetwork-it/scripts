#!/bin/bash

MAVEN_REPO="/home/ltrioschi/.m2/repository/it/peernetwork/lr"
DESTINATION_FOLDER="/home/ltrioschi/stuffs/liferay-osgi"

for edition in `ls $MAVEN_REPO`
do
	for file in `find $MAVEN_REPO/$edition -name "*.jar"`
	do
		[[ $file =~ (([0-9]+.)+)jar ]]
		version="${BASH_REMATCH[1]::-1}"
# 		echo MOVING $file TO $DESTINATION_FOLDER/$edition-$version/
		if [ -n "$version" ]
		then
# 			echo MOVING $file TO $DESTINATION_FOLDER/$edition-$version/
			mkdir $DESTINATION_FOLDER/$edition-$version
			cp -p $file $DESTINATION_FOLDER/$edition-$version/
		fi
	done
done

# read;
exit 0
