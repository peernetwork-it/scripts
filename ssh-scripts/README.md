# pnssh
## Introduzione

Questo script è stato realizzato per automatizzare la gestione del collegamento tramite SSH ai sistemi clienti.

## Prerequisiti

Lo script si basa su tre prerequisiti:
- Essere collegati via VPN al cliente con cui si vuole lavorare
- Aver generato una chiave pubblica-privata RSA sulla propria macchina locale e averla caricata sui server clienti nell'`authorized_keys` di SSH
- Aver popolato il proprio file `/etc/hosts` con i nomi dei server clienti

## Installazione

Per utilizzare lo script `pnssh` occorre prima eseguire il script di configurazione `load-pnssh.sh` presente in questo repository eseguendo il comando da `root` o `sudoer` `./load-pnssh.sh`; è necessario aver tirato giù anche il file `pn-hosts`.

Questo comando oltre a risolvere il terzo punto dei prerequisiti, va ad installare lo script `pnssh` nella propria cartella `/usr/bin` in modo da poter eseguire lo script in qualsiasi path in cui ci si trovi.

## Utilizzo
Il pattern di utilizzo dello script è il seguente:
`pnssh customerserver`

Qualora non sia presente la propria chiave RSA pubblica nelle `authorized_keys` del sistema cliente verrà richiesta la password di `root` (perdendo però così gran parte della comodità dello script).

`customerserver` è il nome del server cliente; questi sono stati caricati tramite il file `pnhosts` nel proprio `/etc/hosts/` (e.g. `bayker-esitest`, `ccci-lrprd` )

## Maintenance

Per mantenere aggiornato lo script e condividere le modifiche con i propri colleghi, alla disponibilità di un nuovo sistema/modifica di uno esistente occorre:
- Modificare il file `pnhosts` collegato al repository Git
- Modificare lo script `pnssh` in caso di aggiunta di un nuovo sistema
- Ricaricare le modifiche tramite il lancio da `root` o `sudoer` di `./load-pnssh.sh`
- Testare l'accesso al nuovo sistema
- Committare e pushare le modifiche su Git

Non lavorare mai direttamente sul proprio file `/etc/hosts` o direttamente sullo script `/usr/bin/pnssh`


